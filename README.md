# osim-utils

This package contains resources that are useful to more than one OSIM project. It includes, for
example, API clients to common literature resources, such as Europe PMC.

## Installation

```
pip install osim-utils
```
