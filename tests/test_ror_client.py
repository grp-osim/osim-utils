def test_get_organisation_by_id_ok(ror_client):
    r = ror_client.get_organisation_by_id("04m01e293")
    assert r["id"] == "https://ror.org/04m01e293"


def test_get_organisation_by_url_ok(ror_client):
    r = ror_client.get_organisation_by_id("https://ror.org/04m01e293")
    assert r["name"] == "University of York"
