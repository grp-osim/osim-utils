import local.config as conf
from osim_utils.clients.sherpa import SherpaClient
from test_aux.test_data.sherpa_results_0 import SherpaResults0


def test_sherpa_client_init_with_key():
    sc = SherpaClient(api_key=conf.sherpa_key)
    assert sc.base_endpoint == "https://v2.sherpa.ac.uk/cgi/retrieve"


def test_get_journal_by_issn_str(sherpa_client):
    journal = sherpa_client.get_journal_by_issn(issn="1932-6203")
    assert journal == SherpaResults0.journal_0


def test_get_journal_by_issn_list(sherpa_client):
    journal = sherpa_client.get_journal_by_issn(issn=["1087-0156", "1546-1696"])
    assert journal["title"][0]["title"] == SherpaResults0.journal_1["title"][0]["title"]


def test_get_journal_by_issn_invalid(sherpa_client):
    journal = sherpa_client.get_journal_by_issn(issn="invalid issn")
    assert journal == dict()


def test_get_journal_by_issn_valid_and_invalid(sherpa_client):
    journal = sherpa_client.get_journal_by_issn(issn=["invalid issn", "1546-1696"])
    assert journal["title"][0]["title"] == SherpaResults0.journal_1["title"][0]["title"]


def test_get_journal_by_title(sherpa_client):
    journal = sherpa_client.get_journal_by_title(title="Trends in Cell Biology")
    assert journal["title"][0]["title"] == SherpaResults0.journal_2["title"][0]["title"]


def test_get_journal_by_title_not_found(sherpa_client):
    journal = sherpa_client.get_journal_by_title(
        title="Journal of Procrastination Studies"
    )
    assert journal == dict()
