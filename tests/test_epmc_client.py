def test_query_doi_ok(empc_client):
    r = empc_client.query_doi("10.1093/bioinformatics/btac311")
    assert "PMC9191204" == r["pmcid"]


def test_query_doi_not_exist(empc_client):
    r = empc_client.query_doi("nonsense")
    assert r == dict()


def test_query_doi_list(empc_client):
    doi_list = [
        "10.1101/2022.11.11.516098",
        "10.1016/bs.mie.2022.08.008",
        "10.1242/bio.059561",
        "10.1093/bioinformatics/btac749",
        "10.1021/jacs.2c07378",
    ]
    r = empc_client.query_doi_list(doi_list=doi_list)
    expected_response = {
        "10.1016/bs.mie.2022.08.008": {
            "authorString": "Pietras Z, Wood K, Whitten AE, Jeffries CM.",
            "citedByCount": 0,
            "doi": "10.1016/bs.mie.2022.08.008",
            "firstIndexDate": "2022-11-22",
            "firstPublicationDate": "2022-09-22",
            "hasBook": "N",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "N",
            "hasPDF": "N",
            "hasReferences": "N",
            "hasSuppl": "N",
            "hasTMAccessionNumbers": "N",
            "hasTextMinedTerms": "Y",
            "id": "36410948",
            "inEPMC": "N",
            "inPMC": "N",
            "isOpenAccess": "N",
            "journalIssn": "0076-6879; 1557-7988; ",
            "journalTitle": "Methods Enzymol",
            "journalVolume": "677",
            "pageInfo": "157-189",
            "pmid": "36410948",
            "pubType": "research support, non-u.s. gov't; journal article",
            "pubYear": "2022",
            "source": "MED",
            "title": "Technical considerations for "
            "small-angle neutron scattering from "
            "biological macromolecules in "
            "solution: Cross sections, contrasts, "
            "instrument setup and measurement.",
        },
        "10.1021/jacs.2c07378": {
            "authorString": "Zhang L, Lovell S, De Vita E, "
            "Jagtap PKA, Lucy D, Goya Grocin A, "
            "Kjær S, Borg A, Hennig J, Miller "
            "AK, Tate EW.",
            "citedByCount": 0,
            "doi": "10.1021/jacs.2c07378",
            "firstIndexDate": "2022-11-23",
            "firstPublicationDate": "2022-11-22",
            "fullTextIdList": {"fullTextId": ["PMC9756341"]},
            "hasBook": "N",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "hasPDF": "Y",
            "hasReferences": "N",
            "hasSuppl": "Y",
            "hasTMAccessionNumbers": "Y",
            "hasTextMinedTerms": "Y",
            "id": "36413626",
            "inEPMC": "Y",
            "inPMC": "N",
            "isOpenAccess": "Y",
            "issue": "49",
            "journalIssn": "0002-7863; 1520-5126; ",
            "journalTitle": "J Am Chem Soc",
            "journalVolume": "144",
            "pageInfo": "22493-22504",
            "pmcid": "PMC9756341",
            "pmid": "36413626",
            "pubType": "research-article; journal article",
            "pubYear": "2022",
            "source": "MED",
            "title": "A KLK6 Activity-Based Probe Reveals a Role "
            "for KLK6 Activity in Pancreatic Cancer "
            "Cell Invasion.",
            "tmAccessionTypeList": {"accessionType": ["pdb"]},
        },
        "10.1093/bioinformatics/btac749": {
            "authorString": "Yu D, Chojnowski G, Rosenthal M, Kosinski J.",
            "citedByCount": 0,
            "doi": "10.1093/bioinformatics/btac749",
            "firstIndexDate": "2022-11-23",
            "firstPublicationDate": "2022-11-22",
            "hasBook": "N",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "hasPDF": "N",
            "hasReferences": "N",
            "hasSuppl": "N",
            "hasTMAccessionNumbers": "N",
            "hasTextMinedTerms": "Y",
            "id": "36413069",
            "inEPMC": "N",
            "inPMC": "N",
            "isOpenAccess": "N",
            "journalIssn": "1367-4803; 1367-4811; ",
            "journalTitle": "Bioinformatics",
            "pageInfo": "btac749",
            "pmid": "36413069",
            "pubType": "journal article",
            "pubYear": "2022",
            "source": "MED",
            "title": "AlphaPulldown-a Python package "
            "for protein-protein interaction "
            "screens using "
            "AlphaFold-Multimer.",
        },
        "10.1101/2022.11.11.516098": {
            "authorString": "Jagtap PKA, Müller M, Kiss AE, "
            "Thomae AW, Lapouge K, Beck M, "
            "Becker PB, Hennig J.",
            "bookOrReportDetails": {"publisher": "bioRxiv", "yearOfPublication": 2022},
            "citedByCount": 0,
            "doi": "10.1101/2022.11.11.516098",
            "firstIndexDate": "2022-11-13",
            "firstPublicationDate": "2022-11-11",
            "hasBook": "N",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "hasPDF": "N",
            "hasReferences": "N",
            "hasSuppl": "N",
            "hasTMAccessionNumbers": "N",
            "hasTextMinedTerms": "Y",
            "id": "PPR570124",
            "inEPMC": "N",
            "inPMC": "N",
            "isOpenAccess": "N",
            "pubType": "preprint",
            "pubYear": "2022",
            "source": "PPR",
            "title": "Structural basis of RNA-induced "
            "autoregulation of the DExH-type RNA "
            "helicase maleless",
        },
        "10.1242/bio.059561": {
            "authorString": "Ebenezer TE, Low RS, O'Neill EC, "
            "Huang I, DeSimone A, Farrow SC, Field "
            "RA, Ginger ML, Guerrero SA, Hammond "
            "M, Hampl V, Horst G, Ishikawa T, "
            "Karnkowska A, Linton EW, Myler P, "
            "Nakazawa M, Cardol P, Sánchez-Thomas "
            "R, Saville BJ, Shah MR, Simpson AGB, "
            "Sur A, Suzuki K, Tyler KM, Zimba PV, "
            "Hall N, Field MC.",
            "citedByCount": 0,
            "doi": "10.1242/bio.059561",
            "firstIndexDate": "2022-11-23",
            "firstPublicationDate": "2022-11-22",
            "hasBook": "N",
            "hasDbCrossReferences": "N",
            "hasLabsLinks": "Y",
            "hasPDF": "N",
            "hasReferences": "N",
            "hasSuppl": "N",
            "hasTMAccessionNumbers": "N",
            "hasTextMinedTerms": "Y",
            "id": "36412269",
            "inEPMC": "N",
            "inPMC": "N",
            "isOpenAccess": "N",
            "issue": "11",
            "journalIssn": "2046-6390",
            "journalTitle": "Biol Open",
            "journalVolume": "11",
            "pageInfo": "bio059561",
            "pmid": "36412269",
            "pubType": "journal article",
            "pubYear": "2022",
            "source": "MED",
            "title": "Euglena International Network (EIN): Driving "
            "euglenoid biotechnology for the benefit of a "
            "challenged world.",
        },
    }
    expected_ids = {k: v["id"] for k, v in expected_response.items()}
    actual_ids = {k: v["id"] for k, v in r.items()}
    assert actual_ids == expected_ids
