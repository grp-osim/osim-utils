def test_get_work_by_doi(open_alex_client):
    oac = open_alex_client
    r = oac.get_work_by_doi(doi="10.1038/s41467-022-30388-3")
    expected_id = "https://openalex.org/W4280521901"
    assert r["id"] == expected_id


def test_get_works_by_doi_list(open_alex_client):
    oac = open_alex_client
    doi_list = [
        "10.1101/2022.11.11.516098",
        "10.1021/jacs.2c07378",
        "10.1007/s00249-022-01620-1",
        "10.1038/s41586-022-05455-w",
        "10.1186/s40246-022-00429-5",
        "10.1182/blood.2021013472",
        "10.1093/nar/gkac1011",
        "10.1016/j.molcel.2022.10.010",
        "10.1039/d2sc02794a",
        "10.3390/cancers14092176",
        "10.1097/j.pain.0000000000002481",
        "10.1128/mbio.00157-22",
        "10.1016/j.cub.2022.08.065",
        "10.1093/bioinformatics/btac458",
        "10.1101/2022.04.25.489364",
        "10.1016/j.stem.2022.04.007",
        "10.1007/978-1-0716-2095-3_1",
        "10.1016/j.jbc.2022.101985",
        "10.1039/D2RA90102A",
        "10.1016/S2666-5247(22)00181-1",
        "10.1016/j.jbc.2022.102495",
        "10.1093/nar/gkac928",
        "10.1093/nar/gkac972",
        "10.1038/s41467-022-34004-2",
        "10.1080/15548627.2022.2074105",
        "10.1038/s41467-022-29967-1",
        "10.1007/978-1-0716-1990-2_24",
        "10.1128/mbio.01654-22",
        "10.1016/j.sbi.2022.102488",
        "10.1016/j.ccell.2022.09.017",
        "10.12688/f1000research.110194.1",
        "10.1038/s41580-022-00476-9",
        "10.1107/S205979832200328X",
        "10.1126/sciadv.abn5725",
        "10.1021/jacs.2c05030",
        "10.1186/s12916-022-02550-7",
        "10.1186/s13073-022-01118-7",
        "10.3389/fcell.2022.909424",
        "10.3390/v14040669",
        "10.1038/s41586-022-04618-z",
        "10.1016/j.dnarep.2022.103392",
        "10.3389/fcimb.2022.1039887",
        "10.1093/database/baac087",
        "10.1093/genetics/iyac003",
        "10.1083/jcb.202110044",
        "10.1038/s42003-022-03238-7",
        "10.1038/s41556-022-00878-z",
        "10.1016/j.str.2022.03.011",
        "10.15252/msb.202211186",
        "10.1101/2022.02.16.480740",
        "10.1038/s41416-022-01990-5",
        "10.1242/jcs.259498",
        "10.1016/j.jsb.2022.107852",
        "10.1038/s41467-022-29450-x",
        "10.1038/s41375-022-01687-x",
        "10.1038/s41467-022-33386-7",
        "10.1016/j.isci.2022.105062",
        "10.3390/nano12060893",
        "10.1111/cts.13302",
        "10.1557/s43577-022-00282-w",
        "10.1371/journal.pone.0273698",
        "10.1038/s41598-022-09188-8",
        "10.15252/embj.2021109352",
        "10.1038/s41598-022-08245-6",
        "10.1242/dev.200889",
        "10.1016/j.yjsbx.2022.100072",
        "10.3390/ijms23179951",
        "10.1016/j.celrep.2022.111316",
        "10.1080/21541264.2022.2047583",
        "10.1038/s41467-022-32805-z",
        "10.1021/acs.biomac.2c00582",
        "10.1016/j.str.2022.05.019",
        "10.3389/fcell.2022.991664",
        "10.3389/fphys.2022.946682",
        "10.15252/embj.2021109992",
        "10.1016/j.celrep.2022.110435",
        "10.1039/d2cp02063g",
        "10.3390/pathogens11080856",
        "10.3390/ijms23168877",
        "10.31219/osf.io/8zj9w",
        "10.3389/fmolb.2022.831740",
        "10.1126/science.abi9591",
        "10.1101/gr.276059.121",
        "10.1371/journal.ppat.1010771",
        "10.7554/eLife.69244",
        "10.1016/j.molcel.2022.07.015",
        "10.1107/S1600577522000984",
        "10.1186/s12864-022-08367-1",
        "10.3389/fcell.2022.813314",
        "10.1016/j.isci.2022.103971",
        "10.3389/fimmu.2022.812899",
        "10.1016/j.jacl.2022.04.005",
        "10.1038/s41586-022-04799-7",
        "10.15252/msb.202110855",
        "10.1016/j.chom.2022.07.010",
        "10.1101/gr.275790.121",
        "10.1097/HS9.0000000000000688",
        "10.1186/s13059-022-02623-z",
        "10.1002/advs.202201212",
        "10.1093/cvr/cvac128",
    ]
    r = oac.get_works_by_doi_list(dois=doi_list)
    # 1 doi not returning a OpenAlex result at present
    assert len(r["results"]) == 99


def test_get_embl_works(open_alex_client):
    oac = open_alex_client
    r = oac.get_embl_works(filters={"publication_date": "2022-01-09"})
    assert r["results"][0]["id"] == "https://openalex.org/W4205884634"
    assert r["meta"]["page"] == 1
    assert r["meta"]["per_page"] == 25


def test_get_embl_works_large_per_page(open_alex_client):
    oac = open_alex_client
    r = oac.get_embl_works(
        filters={"publication_date": "2022-01-09"}, params={"per-page": 200}
    )
    assert r["meta"]["per_page"] == 200


def test_get_embl_works_basic_pagination(open_alex_client):
    oac = open_alex_client
    r = oac.get_embl_works(
        filters={"publication_date": "2018-01-01"}, params={"per-page": 1, "page": 10}
    )
    assert r["results"][0]["id"] == "https://openalex.org/W2783754436"
    r = oac.get_embl_works(
        filters={"publication_date": "2018-01-01"}, params={"per-page": 1, "page": 100}
    )
    assert len(r["results"]) == 0


def test_get_all_embl_works(open_alex_client):
    oac = open_alex_client
    r = oac.get_all_embl_works(
        filters={"publication_date": "2018-01-02"}, params={"per-page": 1}
    )
    assert r["meta"]["per_page"] == 1
    assert len(r["results"]) == 2


def test_get_works_by_orcids_ok(open_alex_client):
    r = open_alex_client.get_works_by_orcids(["0000-0002-1298-3089"])
    assert len(r["results"]) >= 9


def test_get_works_by_multiple_orcids_ok(open_alex_client):
    r = open_alex_client.get_works_by_orcids(
        [
            "0000-0003-3508-602X",
            "0000-0003-1982-9145",
        ]
    )
    assert len(r["results"]) >= 13


def test_get_works_by_orcids_not_found(open_alex_client):
    r = open_alex_client.get_works_by_orcids(["9999-0003-0682-1646"])
    assert len(r["results"]) == 0


def test_get_works_by_orcids_additional_filters_ok(open_alex_client):
    r = open_alex_client.get_works_by_orcids(
        ["0000-0003-3508-602X"],
        filters={"publication_date": "2014-07-01"},
    )
    assert len(r["results"]) == 1
