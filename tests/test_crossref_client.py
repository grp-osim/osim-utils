def test_get_metadata_by_doi(crossref_client):
    r = crossref_client.get_metadata_by_doi("10.1002/9781118970195.ch11")
    assert r["created"]["publisher"] == "Wiley"
