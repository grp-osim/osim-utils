import pytest

import local.config as conf
from osim_utils.clients.crossref import CrossRefClient
from osim_utils.clients.embl_data_warehouse import DataWareClient
from osim_utils.clients.epmc import EpmcClient
from osim_utils.clients.openalex import OpenAlexClient
from osim_utils.clients.ror import RorClient
from osim_utils.clients.sherpa import SherpaClient

# region ROR
@pytest.fixture
def ror_client():
    return RorClient()


# endregion


# region Data warehouse
@pytest.fixture
def warehouse_client():
    return DataWareClient()


# endregion


# region EpmcClient
@pytest.fixture
def empc_client():
    return EpmcClient()


# endregion

# region OpenAlex
@pytest.fixture
def open_alex_client():
    return OpenAlexClient()


# endregion


# region Sherpa
@pytest.fixture
def sherpa_client():
    return SherpaClient()


# endregion

# region Crossref
@pytest.fixture
def crossref_client():
    return CrossRefClient()


# endregion
