def test_get_all_publications_ok(warehouse_client):
    r = warehouse_client.get_all_publications()
    ids = [x["id"] for x in r]
    # check there are no repeated DOIs in response
    assert len(ids) == len(set(ids))
    assert "2003789299" in ids
