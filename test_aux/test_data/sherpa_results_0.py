class SherpaResults0:
    journal_0 = {
        "type_phrases": [{"value": "journal", "phrase": "Journal", "language": "en"}],
        "publishers": [
            {
                "relationship_type_phrases": [
                    {
                        "phrase": "Commercial Publisher",
                        "value": "commercial_publisher",
                        "language": "en",
                    }
                ],
                "relationship_type": "commercial_publisher",
                "publisher": {
                    "uri": "https://v2.sherpa.ac.uk/id/publisher/112",
                    "name": [
                        {
                            "name": "Public Library of Science",
                            "language": "en",
                            "language_phrases": [
                                {"language": "en", "phrase": "English", "value": "en"}
                            ],
                            "preferred_phrases": [
                                {"language": "en", "phrase": "Name", "value": "name"}
                            ],
                            "preferred": "name",
                        }
                    ],
                    "country": "us",
                    "publication_count": 12,
                    "country_phrases": [
                        {
                            "phrase": "United States of America",
                            "value": "us",
                            "language": "en",
                        }
                    ],
                    "identifiers": [
                        {
                            "type": "ror",
                            "identifier": "https://ror.org/008zgvp64",
                            "type_phrases": [
                                {"phrase": "ROR ID", "value": "ror", "language": "en"}
                            ],
                        }
                    ],
                    "id": 112,
                    "url": "https://plos.org/",
                },
            }
        ],
        "publisher_policy": [
            {
                "id": 112,
                "urls": [
                    {
                        "url": "https://plos.org/open-science/why-open-access/",
                        "description": "Benefits of Open",
                    }
                ],
                "permitted_oa": [
                    {
                        "id": 2726,
                        "additional_oa_fee": "no",
                        "copyright_owner": "authors",
                        "license": [
                            {
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY",
                                        "value": "cc_by",
                                        "language": "en",
                                    }
                                ],
                                "version": "4",
                                "license": "cc_by",
                            }
                        ],
                        "article_version": ["submitted"],
                        "additional_oa_fee_phrases": [
                            {"phrase": "No", "value": "no", "language": "en"}
                        ],
                        "conditions": [
                            "Published source must be acknowledged with citation"
                        ],
                        "location": {
                            "location": ["preprint_repository"],
                            "location_phrases": [
                                {
                                    "language": "en",
                                    "value": "preprint_repository",
                                    "phrase": "Preprint Repository",
                                }
                            ],
                        },
                        "article_version_phrases": [
                            {
                                "value": "submitted",
                                "phrase": "Submitted",
                                "language": "en",
                            }
                        ],
                        "copyright_owner_phrases": [
                            {"phrase": "Authors", "value": "authors", "language": "en"}
                        ],
                    },
                    {
                        "id": 2727,
                        "article_version": ["accepted"],
                        "copyright_owner": "authors",
                        "license": [
                            {
                                "license": "cc_by",
                                "version": "4",
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY",
                                        "value": "cc_by",
                                        "language": "en",
                                    }
                                ],
                            }
                        ],
                        "additional_oa_fee": "no",
                        "location": {
                            "location_phrases": [
                                {
                                    "language": "en",
                                    "value": "any_website",
                                    "phrase": "Any Website",
                                }
                            ],
                            "location": ["any_website"],
                        },
                        "additional_oa_fee_phrases": [
                            {"phrase": "No", "value": "no", "language": "en"}
                        ],
                        "copyright_owner_phrases": [
                            {"language": "en", "value": "authors", "phrase": "Authors"}
                        ],
                        "article_version_phrases": [
                            {
                                "phrase": "Accepted",
                                "value": "accepted",
                                "language": "en",
                            }
                        ],
                    },
                    {
                        "id": 2728,
                        "publisher_deposit": [
                            {
                                "system_metadata": {
                                    "id": 267,
                                    "uri": "https://v2.sherpa.ac.uk/id/repository/267",
                                },
                                "repository_metadata": {
                                    "name": [
                                        {
                                            "preferred": "name",
                                            "language_phrases": [
                                                {
                                                    "language": "en",
                                                    "phrase": "English",
                                                    "value": "en",
                                                }
                                            ],
                                            "preferred_phrases": [
                                                {
                                                    "phrase": "Name",
                                                    "value": "name",
                                                    "language": "en",
                                                }
                                            ],
                                            "language": "en",
                                            "name": "PubMed Central",
                                        }
                                    ],
                                    "type": "disciplinary",
                                    "type_phrases": [
                                        {
                                            "phrase": "Disciplinary",
                                            "value": "disciplinary",
                                            "language": "en",
                                        }
                                    ],
                                    "url": "http://www.ncbi.nlm.nih.gov/pmc/",
                                },
                            }
                        ],
                        "article_version": ["published"],
                        "license": [
                            {
                                "license": "cc_by",
                                "version": "4.0",
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY",
                                        "value": "cc_by",
                                        "language": "en",
                                    }
                                ],
                            }
                        ],
                        "copyright_owner": "authors",
                        "additional_oa_fee": "no",
                        "location": {
                            "location_phrases": [
                                {
                                    "value": "any_website",
                                    "phrase": "Any Website",
                                    "language": "en",
                                },
                                {
                                    "value": "named_repository",
                                    "phrase": "Named Repository",
                                    "language": "en",
                                },
                                {
                                    "language": "en",
                                    "phrase": "Journal Website",
                                    "value": "this_journal",
                                },
                            ],
                            "location": [
                                "any_website",
                                "named_repository",
                                "this_journal",
                            ],
                            "named_repository": ["PubMed Central"],
                        },
                        "conditions": [
                            "Published source must be acknowledged with citation"
                        ],
                        "additional_oa_fee_phrases": [
                            {"phrase": "No", "value": "no", "language": "en"}
                        ],
                        "copyright_owner_phrases": [
                            {"value": "authors", "phrase": "Authors", "language": "en"}
                        ],
                        "article_version_phrases": [
                            {
                                "value": "published",
                                "phrase": "Published",
                                "language": "en",
                            }
                        ],
                    },
                ],
                "publication_count": 12,
                "open_access_prohibited_phrases": [
                    {"language": "en", "phrase": "No", "value": "no"}
                ],
                "internal_moniker": "Default policy",
                "open_access_prohibited": "no",
                "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/112",
            }
        ],
        "issns": [
            {
                "type": "electronic",
                "issn": "1932-6203",
                "type_phrases": [
                    {"value": "electronic", "phrase": "Electronic", "language": "en"}
                ],
            }
        ],
        "listed_in_doaj_phrases": [{"language": "en", "value": "yes", "phrase": "Yes"}],
        "title": [
            {
                "language": "en",
                "title": "PLoS ONE",
                "language_phrases": [
                    {"language": "en", "phrase": "English", "value": "en"}
                ],
                "preferred_phrases": [
                    {"value": "name", "phrase": "Title", "language": "en"}
                ],
                "preferred": "name",
            }
        ],
        "system_metadata": {
            "publicly_visible": "yes",
            "date_created": "2010-09-24 15:57:22",
            "date_modified": "2022-11-18 08:56:58",
            "id": 17599,
            "publicly_visible_phrases": [
                {"value": "yes", "phrase": "Yes", "language": "en"}
            ],
            "uri": "https://v2.sherpa.ac.uk/id/publication/17599",
        },
        "listed_in_doaj": "yes",
        "id": 17599,
        "url": "http://www.plosone.org/",
        "type": "journal",
    }
    journal_1 = {
        "publishers": [
            {
                "relationship_type_phrases": [
                    {
                        "language": "en",
                        "value": "commercial_publisher",
                        "phrase": "Commercial Publisher",
                    }
                ],
                "publisher": {
                    "url": "https://www.nature.com/",
                    "id": 3286,
                    "country_phrases": [
                        {"value": "gb", "phrase": "United Kingdom", "language": "en"}
                    ],
                    "publication_count": 91,
                    "country": "gb",
                    "uri": "https://v2.sherpa.ac.uk/id/publisher/3286",
                    "name": [
                        {
                            "preferred": "name",
                            "preferred_phrases": [
                                {"language": "en", "value": "name", "phrase": "Name"}
                            ],
                            "language_phrases": [
                                {"language": "en", "phrase": "English", "value": "en"}
                            ],
                            "language": "en",
                            "name": "Nature Research",
                        }
                    ],
                    "imprint_of_id": 62037,
                },
                "relationship_type": "commercial_publisher",
            }
        ],
        "issns": [
            {
                "type": "print",
                "type_phrases": [
                    {"phrase": "Print", "value": "print", "language": "en"}
                ],
                "issn": "1087-0156",
            },
            {
                "type": "electronic",
                "type_phrases": [
                    {"value": "electronic", "phrase": "Electronic", "language": "en"}
                ],
                "issn": "1546-1696",
            },
        ],
        "tj_status_phrases": [
            {"phrase": "Plan S Approved", "value": "plan_s_approved", "language": "en"},
            {"language": "en", "value": "jisc_approved", "phrase": "Jisc Approved"},
        ],
        "type_phrases": [{"language": "en", "value": "journal", "phrase": "Journal"}],
        "listed_in_doaj": "no",
        "system_metadata": {
            "date_created": "2010-07-16 12:09:31",
            "publicly_visible": "yes",
            "date_modified": "2022-08-19 08:36:41",
            "id": 1643,
            "uri": "https://v2.sherpa.ac.uk/id/publication/1643",
            "publicly_visible_phrases": [
                {"value": "yes", "phrase": "Yes", "language": "en"}
            ],
        },
        "title": [
            {
                "language_phrases": [
                    {"value": "en", "phrase": "English", "language": "en"}
                ],
                "title": "Nature Biotechnology",
                "language": "en",
            }
        ],
        "listed_in_doaj_phrases": [{"language": "en", "phrase": "No", "value": "no"}],
        "publisher_policy": [
            {
                "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/3286",
                "open_access_prohibited": "no",
                "internal_moniker": "Default Policy",
                "open_access_prohibited_phrases": [
                    {"phrase": "No", "value": "no", "language": "en"}
                ],
                "publication_count": 38,
                "permitted_oa": [
                    {
                        "copyright_owner_phrases": [
                            {"language": "en", "value": "authors", "phrase": "Authors"}
                        ],
                        "article_version_phrases": [
                            {
                                "language": "en",
                                "value": "submitted",
                                "phrase": "Submitted",
                            }
                        ],
                        "location": {
                            "location": [
                                "authors_homepage",
                                "funder_designated_location",
                                "institutional_repository",
                                "preprint_repository",
                            ],
                            "location_phrases": [
                                {
                                    "phrase": "Author's Homepage",
                                    "value": "authors_homepage",
                                    "language": "en",
                                },
                                {
                                    "phrase": "Funder Designated Location",
                                    "value": "funder_designated_location",
                                    "language": "en",
                                },
                                {
                                    "language": "en",
                                    "value": "institutional_repository",
                                    "phrase": "Institutional Repository",
                                },
                                {
                                    "language": "en",
                                    "value": "preprint_repository",
                                    "phrase": "Preprint Repository",
                                },
                            ],
                        },
                        "conditions": [
                            "Must link to publisher version",
                            "Upon publication, source must be acknowledged and DOI cited",
                            "Post-prints are subject to Springer Nature re-use terms",
                            "Non-commercial use only",
                        ],
                        "additional_oa_fee_phrases": [
                            {"language": "en", "value": "no", "phrase": "No"}
                        ],
                        "article_version": ["submitted"],
                        "copyright_owner": "authors",
                        "additional_oa_fee": "no",
                        "prerequisites": {
                            "prerequisites": ["when_research_article"],
                            "prerequisites_phrases": [
                                {
                                    "language": "en",
                                    "value": "when_research_article",
                                    "phrase": "If a Research Article",
                                }
                            ],
                        },
                        "id": 84,
                    },
                    {
                        "prerequisites": {
                            "prerequisites": ["when_research_article"],
                            "prerequisites_phrases": [
                                {
                                    "language": "en",
                                    "value": "when_research_article",
                                    "phrase": "If a Research Article",
                                }
                            ],
                        },
                        "id": 85,
                        "copyright_owner": "authors",
                        "license": [
                            {
                                "license_phrases": [
                                    {
                                        "language": "en",
                                        "phrase": "Publisher's Bespoke License",
                                        "value": "bespoke_license",
                                    }
                                ],
                                "license": "bespoke_license",
                            }
                        ],
                        "article_version": ["accepted"],
                        "additional_oa_fee": "no",
                        "conditions": [
                            "Must link to publisher version",
                            "Published source must be acknowledged and DOI cited",
                            "Post-prints are subject to Springer Nature re-use terms",
                            "Non-commercial use only",
                        ],
                        "location": {
                            "location_phrases": [
                                {
                                    "phrase": "Author's Homepage",
                                    "value": "authors_homepage",
                                    "language": "en",
                                },
                                {
                                    "phrase": "Funder Designated Location",
                                    "value": "funder_designated_location",
                                    "language": "en",
                                },
                                {
                                    "language": "en",
                                    "phrase": "Institutional Repository",
                                    "value": "institutional_repository",
                                },
                                {
                                    "phrase": "Named Repository",
                                    "value": "named_repository",
                                    "language": "en",
                                },
                            ],
                            "named_repository": ["PubMed Central", "Europe PMC"],
                            "location": [
                                "authors_homepage",
                                "funder_designated_location",
                                "institutional_repository",
                                "named_repository",
                            ],
                        },
                        "embargo": {
                            "amount": 6,
                            "units_phrases": [
                                {
                                    "value": "months",
                                    "phrase": "Months",
                                    "language": "en",
                                }
                            ],
                            "units": "months",
                        },
                        "additional_oa_fee_phrases": [
                            {"value": "no", "phrase": "No", "language": "en"}
                        ],
                        "article_version_phrases": [
                            {
                                "phrase": "Accepted",
                                "value": "accepted",
                                "language": "en",
                            }
                        ],
                        "copyright_owner_phrases": [
                            {"phrase": "Authors", "value": "authors", "language": "en"}
                        ],
                    },
                ],
                "id": 3286,
                "urls": [
                    {
                        "url": "https://www.nature.com/neuro/editorial-policies/self-archiving-and-license-to-publish",
                        "description": "Self archiving and license to publish",
                    },
                    {
                        "description": "Preprints and Conference Proceedings",
                        "url": "https://www.nature.com/nature-portfolio/editorial-policies/preprints-and-conference-proceedings",
                    },
                    {
                        "description": "Accepted manuscript terms of use",
                        "url": "https://www.springernature.com/gp/open-research/policies/accepted-manuscript-terms",
                    },
                ],
            },
            {
                "open_access_prohibited": "no",
                "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/4410",
                "id": 4410,
                "urls": [
                    {
                        "url": "https://www.springernature.com/gp/open-research/about/the-fundamentals-of-open-access-and-open-research",
                        "description": "The fundamentals of open access and open research",
                    },
                    {
                        "url": "https://www.nature.com/neuro/editorial-policies/self-archiving-and-license-to-publish",
                        "description": "Self archiving and license to publish",
                    },
                    {
                        "url": "https://www.springernature.com/gp/open-research/policies/journal-policies",
                        "description": "Open access policies for journals",
                    },
                ],
                "permitted_oa": [
                    {
                        "additional_oa_fee_phrases": [
                            {"language": "en", "value": "yes", "phrase": "Yes"}
                        ],
                        "location": {
                            "location": ["any_website", "this_journal"],
                            "location_phrases": [
                                {
                                    "value": "any_website",
                                    "phrase": "Any Website",
                                    "language": "en",
                                },
                                {
                                    "phrase": "Journal Website",
                                    "value": "this_journal",
                                    "language": "en",
                                },
                            ],
                        },
                        "conditions": [
                            "Published source must be acknowledged with citation"
                        ],
                        "copyright_owner_phrases": [
                            {"phrase": "Authors", "value": "authors", "language": "en"}
                        ],
                        "article_version_phrases": [
                            {
                                "phrase": "Published",
                                "value": "published",
                                "language": "en",
                            }
                        ],
                        "id": 6064,
                        "publisher_deposit": [
                            {
                                "system_metadata": {
                                    "id": 908,
                                    "uri": "https://v2.sherpa.ac.uk/id/repository/908",
                                },
                                "repository_metadata": {
                                    "type": "disciplinary",
                                    "notes": "Launched as UK PubMed Central (UKPMC) in January 2007, changed to Europe PubMed Central in November 2012.\r\nSpecial item types include: Links",
                                    "name": [
                                        {
                                            "language_phrases": [
                                                {
                                                    "value": "en",
                                                    "phrase": "English",
                                                    "language": "en",
                                                }
                                            ],
                                            "preferred_phrases": [
                                                {
                                                    "language": "en",
                                                    "phrase": "Name",
                                                    "value": "name",
                                                }
                                            ],
                                            "preferred": "name",
                                            "name": "Europe PMC",
                                            "language": "en",
                                        }
                                    ],
                                    "type_phrases": [
                                        {
                                            "language": "en",
                                            "phrase": "Disciplinary",
                                            "value": "disciplinary",
                                        }
                                    ],
                                    "url": "http://europepmc.org/",
                                },
                            },
                            {
                                "repository_metadata": {
                                    "type": "disciplinary",
                                    "name": [
                                        {
                                            "name": "PubMed Central",
                                            "language": "en",
                                            "preferred_phrases": [
                                                {
                                                    "language": "en",
                                                    "value": "name",
                                                    "phrase": "Name",
                                                }
                                            ],
                                            "language_phrases": [
                                                {
                                                    "value": "en",
                                                    "phrase": "English",
                                                    "language": "en",
                                                }
                                            ],
                                            "preferred": "name",
                                        }
                                    ],
                                    "url": "http://www.ncbi.nlm.nih.gov/pmc/",
                                    "type_phrases": [
                                        {
                                            "language": "en",
                                            "phrase": "Disciplinary",
                                            "value": "disciplinary",
                                        }
                                    ],
                                },
                                "system_metadata": {
                                    "id": 267,
                                    "uri": "https://v2.sherpa.ac.uk/id/repository/267",
                                },
                            },
                        ],
                        "additional_oa_fee": "yes",
                        "article_version": ["published"],
                        "license": [
                            {
                                "license_phrases": [
                                    {
                                        "language": "en",
                                        "value": "cc_by",
                                        "phrase": "CC BY",
                                    }
                                ],
                                "version": "4.0",
                                "license": "cc_by",
                            }
                        ],
                        "copyright_owner": "authors",
                    }
                ],
                "publication_count": 35,
                "open_access_prohibited_phrases": [
                    {"phrase": "No", "value": "no", "language": "en"}
                ],
                "internal_moniker": "Open Access",
            },
            {
                "open_access_prohibited_phrases": [
                    {"language": "en", "value": "no", "phrase": "No"}
                ],
                "publication_count": 35,
                "internal_moniker": "UKRI",
                "urls": [
                    {
                        "url": "https://www.springernature.com/gp/open-research/plan-s-compliance",
                        "description": "Plan S compliance for Springer Nature authors",
                    }
                ],
                "id": 6046,
                "permitted_oa": [
                    {
                        "article_version_phrases": [
                            {
                                "language": "en",
                                "phrase": "Accepted",
                                "value": "accepted",
                            }
                        ],
                        "additional_oa_fee_phrases": [
                            {"language": "en", "value": "no", "phrase": "No"}
                        ],
                        "location": {
                            "location_phrases": [
                                {
                                    "language": "en",
                                    "value": "any_website",
                                    "phrase": "Any Website",
                                },
                                {
                                    "language": "en",
                                    "value": "funder_designated_location",
                                    "phrase": "Funder Designated Location",
                                },
                                {
                                    "language": "en",
                                    "phrase": "Institutional Repository",
                                    "value": "institutional_repository",
                                },
                            ],
                            "location": [
                                "any_website",
                                "funder_designated_location",
                                "institutional_repository",
                            ],
                        },
                        "conditions": ["Deposit permitted upon publication of record"],
                        "additional_oa_fee": "no",
                        "article_version": ["accepted"],
                        "public_notes": [
                            "Authors must upon editorial acceptance, request a UKRI exceptional publishing agreement from the journal"
                        ],
                        "license": [
                            {
                                "license": "cc_by",
                                "license_phrases": [
                                    {
                                        "language": "en",
                                        "phrase": "CC BY",
                                        "value": "cc_by",
                                    }
                                ],
                                "version": "4.0",
                            }
                        ],
                        "id": 8637,
                        "prerequisites": {
                            "prerequisite_funders": [
                                {
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/1292",
                                        "id": 1292,
                                    },
                                    "funder_metadata": {
                                        "country": "gb",
                                        "groups": [
                                            {
                                                "id": 1061,
                                                "name": "UK Research and Innovation",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1061",
                                                "type": "funder_group",
                                            },
                                            {
                                                "name": "Plan S Funders",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1063",
                                                "type": "funder_group",
                                                "id": 1063,
                                            },
                                        ],
                                        "name": [
                                            {
                                                "language": "en",
                                                "name": "UK Research and Innovation",
                                                "preferred_phrases": [
                                                    {
                                                        "language": "en",
                                                        "phrase": "Acronym",
                                                        "value": "acronym",
                                                    }
                                                ],
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "phrase": "English",
                                                        "language": "en",
                                                    }
                                                ],
                                                "acronym": "UKRI",
                                                "preferred": "acronym",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "type": "ror",
                                                "identifier": "https://ror.org/001aqnf71",
                                                "type_phrases": [
                                                    {
                                                        "value": "ror",
                                                        "phrase": "ROR ID",
                                                        "language": "en",
                                                    }
                                                ],
                                            }
                                        ],
                                        "id": 1292,
                                        "url": [
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "phrase": "English",
                                                        "value": "en",
                                                    }
                                                ],
                                                "url": "https://www.ukri.org/",
                                            }
                                        ],
                                        "country_phrases": [
                                            {
                                                "language": "en",
                                                "value": "gb",
                                                "phrase": "United Kingdom",
                                            }
                                        ],
                                    },
                                }
                            ]
                        },
                    }
                ],
                "open_access_prohibited": "no",
                "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/6046",
            },
        ],
        "type": "journal",
        "id": 1643,
        "url": "http://www.nature.com/nbt/",
        "tj_status": ["plan_s_approved", "jisc_approved"],
    }
    journal_2 = {
        "id": 23055,
        "url": "https://www.cell.com/trends/cell-biology/home",
        "listed_in_doaj_phrases": [{"phrase": "No", "language": "en", "value": "no"}],
        "title": [
            {
                "language": "en",
                "language_phrases": [
                    {"phrase": "English", "language": "en", "value": "en"}
                ],
                "title": "Trends in Cell Biology",
            }
        ],
        "issns": [
            {
                "type_phrases": [
                    {"language": "en", "value": "print", "phrase": "Print"}
                ],
                "type": "print",
                "issn": "0962-8924",
            },
            {
                "type": "electronic",
                "type_phrases": [
                    {"value": "electronic", "language": "en", "phrase": "Electronic"}
                ],
                "issn": "1879-3088",
            },
        ],
        "publishers": [
            {
                "relationship_type": "commercial_publisher",
                "relationship_type_phrases": [
                    {
                        "phrase": "Commercial Publisher",
                        "value": "commercial_publisher",
                        "language": "en",
                    }
                ],
                "publisher": {
                    "country_phrases": [
                        {
                            "language": "en",
                            "value": "us",
                            "phrase": "United States of America",
                        }
                    ],
                    "imprints_id": [131, 265, 61607],
                    "publication_count": 2335,
                    "country": "us",
                    "url": "http://www.elsevier.com/",
                    "id": 30,
                    "uri": "https://v2.sherpa.ac.uk/id/publisher/30",
                    "name": [
                        {
                            "name": "Elsevier",
                            "preferred": "name",
                            "preferred_phrases": [
                                {"phrase": "Name", "language": "en", "value": "name"}
                            ],
                            "language": "en",
                            "language_phrases": [
                                {"language": "en", "value": "en", "phrase": "English"}
                            ],
                        }
                    ],
                },
            },
            {
                "publisher": {
                    "name": [
                        {
                            "preferred": "name",
                            "name": "Cell Press",
                            "language": "en",
                            "language_phrases": [
                                {"phrase": "English", "language": "en", "value": "en"}
                            ],
                            "preferred_phrases": [
                                {"value": "name", "language": "en", "phrase": "Name"}
                            ],
                        }
                    ],
                    "uri": "https://v2.sherpa.ac.uk/id/publisher/131",
                    "imprint_of_id": 30,
                    "id": 131,
                    "url": "https://www.cell.com/",
                    "country": "us",
                    "publication_count": 41,
                    "country_phrases": [
                        {
                            "phrase": "United States of America",
                            "language": "en",
                            "value": "us",
                        }
                    ],
                },
                "relationship_type_phrases": [
                    {"phrase": "Imprint", "value": "imprint", "language": "en"}
                ],
                "relationship_type": "imprint",
            },
        ],
        "listed_in_doaj": "no",
        "publisher_policy": [
            {
                "internal_moniker": "12 months",
                "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/2823",
                "open_access_prohibited_phrases": [
                    {"value": "no", "language": "en", "phrase": "No"}
                ],
                "open_access_prohibited": "no",
                "urls": [
                    {
                        "description": "Unleashing the power of academic sharing",
                        "url": "http://www.elsevier.com/connect/elsevier-updates-its-policies-perspectives-and-services-on-article-sharing",
                    },
                    {
                        "url": "http://www.elsevier.com/about/company-information/policies/sharing",
                        "description": "Article Sharing",
                    },
                    {
                        "url": "https://www.elsevier.com/about/our-business/policies/sharing/policy-faq",
                        "description": "Sharing and hosting policy FAQ",
                    },
                    {
                        "description": "Open access",
                        "url": "http://www.elsevier.com/about/open-access/green-open-access",
                    },
                    {
                        "description": "Journal Embargo Finder",
                        "url": "https://www.elsevier.com/open-access/journal-embargo-finder",
                    },
                    {
                        "url": "https://www.elsevier.com/__data/assets/pdf_file/0011/78473/UK-Embargo-Periods.pdf",
                        "description": "Journal Embargo List for UK Authors",
                    },
                    {
                        "description": "Open Access Agreements",
                        "url": "https://www.elsevier.com/about/policies/sharing",
                    },
                ],
                "permitted_oa": [
                    {
                        "id": 449,
                        "additional_oa_fee": "no",
                        "location": {
                            "location": ["any_website", "named_repository"],
                            "location_phrases": [
                                {
                                    "phrase": "Any Website",
                                    "language": "en",
                                    "value": "any_website",
                                },
                                {
                                    "language": "en",
                                    "value": "named_repository",
                                    "phrase": "Named Repository",
                                },
                            ],
                            "named_repository": ["arXiv", "RePEC"],
                        },
                        "article_version_phrases": [
                            {
                                "phrase": "Submitted",
                                "language": "en",
                                "value": "submitted",
                            }
                        ],
                        "article_version": ["submitted"],
                        "additional_oa_fee_phrases": [
                            {"phrase": "No", "value": "no", "language": "en"}
                        ],
                    },
                    {
                        "article_version_phrases": [
                            {
                                "phrase": "Accepted",
                                "language": "en",
                                "value": "accepted",
                            }
                        ],
                        "article_version": ["accepted"],
                        "additional_oa_fee_phrases": [
                            {"value": "no", "language": "en", "phrase": "No"}
                        ],
                        "conditions": [
                            "Must link to publisher version with DOI",
                            "Published source must be acknowledged with citation",
                        ],
                        "id": 450,
                        "license": [
                            {
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY-NC-ND",
                                        "value": "cc_by_nc_nd",
                                        "language": "en",
                                    }
                                ],
                                "license": "cc_by_nc_nd",
                            }
                        ],
                        "additional_oa_fee": "no",
                        "location": {
                            "location": ["authors_homepage", "non_commercial_website"],
                            "location_phrases": [
                                {
                                    "value": "authors_homepage",
                                    "language": "en",
                                    "phrase": "Author's Homepage",
                                },
                                {
                                    "language": "en",
                                    "value": "non_commercial_website",
                                    "phrase": "Non-Commercial Website",
                                },
                            ],
                        },
                    },
                    {
                        "article_version_phrases": [
                            {
                                "phrase": "Accepted",
                                "value": "accepted",
                                "language": "en",
                            }
                        ],
                        "article_version": ["accepted"],
                        "additional_oa_fee_phrases": [
                            {"phrase": "No", "language": "en", "value": "no"}
                        ],
                        "id": 451,
                        "conditions": [
                            "Must link to publisher version with DOI",
                            "Published source must be acknowledged with citation",
                        ],
                        "embargo": {
                            "amount": 12,
                            "units": "months",
                            "units_phrases": [
                                {
                                    "phrase": "Months",
                                    "language": "en",
                                    "value": "months",
                                }
                            ],
                        },
                        "license": [
                            {
                                "license_phrases": [
                                    {
                                        "value": "cc_by_nc_nd",
                                        "language": "en",
                                        "phrase": "CC BY-NC-ND",
                                    }
                                ],
                                "license": "cc_by_nc_nd",
                            }
                        ],
                        "location": {
                            "location": [
                                "non_commercial_institutional_repository",
                                "non_commercial_website",
                                "subject_repository",
                            ],
                            "location_phrases": [
                                {
                                    "value": "non_commercial_institutional_repository",
                                    "language": "en",
                                    "phrase": "Non-Commercial Institutional Repository",
                                },
                                {
                                    "value": "non_commercial_website",
                                    "language": "en",
                                    "phrase": "Non-Commercial Website",
                                },
                                {
                                    "language": "en",
                                    "value": "subject_repository",
                                    "phrase": "Subject Repository",
                                },
                            ],
                        },
                        "additional_oa_fee": "no",
                    },
                ],
                "publication_count": 986,
                "id": 2823,
            },
            {
                "open_access_prohibited_phrases": [
                    {"phrase": "No", "value": "no", "language": "en"}
                ],
                "internal_moniker": "Paid Open access option",
                "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/3323",
                "open_access_prohibited": "no",
                "permitted_oa": [
                    {
                        "article_version_phrases": [
                            {
                                "phrase": "Published",
                                "language": "en",
                                "value": "published",
                            }
                        ],
                        "additional_oa_fee_phrases": [
                            {"value": "yes", "language": "en", "phrase": "Yes"}
                        ],
                        "article_version": ["published"],
                        "id": 412,
                        "conditions": [
                            "Published source must be acknowledged with citation"
                        ],
                        "additional_oa_fee": "yes",
                        "location": {
                            "location": [
                                "any_website",
                                "named_repository",
                                "non_commercial_repository",
                                "this_journal",
                            ],
                            "location_phrases": [
                                {
                                    "value": "any_website",
                                    "language": "en",
                                    "phrase": "Any Website",
                                },
                                {
                                    "phrase": "Named Repository",
                                    "language": "en",
                                    "value": "named_repository",
                                },
                                {
                                    "phrase": "Non-Commercial Repository",
                                    "language": "en",
                                    "value": "non_commercial_repository",
                                },
                                {
                                    "phrase": "Journal Website",
                                    "language": "en",
                                    "value": "this_journal",
                                },
                            ],
                            "named_repository": [
                                "PubMed Central",
                                "Research for Development Repository",
                                "ESRC Research Catalogue",
                            ],
                        },
                        "license": [
                            {
                                "license": "cc_by_nc_nd",
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY-NC-ND",
                                        "value": "cc_by_nc_nd",
                                        "language": "en",
                                    }
                                ],
                                "version": "4.0",
                            }
                        ],
                    },
                    {
                        "article_version_phrases": [
                            {
                                "language": "en",
                                "value": "published",
                                "phrase": "Published",
                            }
                        ],
                        "article_version": ["published"],
                        "additional_oa_fee_phrases": [
                            {"phrase": "Yes", "value": "yes", "language": "en"}
                        ],
                        "id": 413,
                        "conditions": [
                            "Published source must be acknowledged with citation"
                        ],
                        "license": [
                            {
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY",
                                        "language": "en",
                                        "value": "cc_by",
                                    }
                                ],
                                "license": "cc_by",
                            }
                        ],
                        "additional_oa_fee": "yes",
                        "location": {
                            "named_repository": [
                                "PubMed Central",
                                "Research for Development Repository",
                                "ESRC Research Catalogue",
                            ],
                            "location_phrases": [
                                {
                                    "value": "any_website",
                                    "language": "en",
                                    "phrase": "Any Website",
                                },
                                {
                                    "language": "en",
                                    "value": "institutional_repository",
                                    "phrase": "Institutional Repository",
                                },
                                {
                                    "language": "en",
                                    "value": "named_repository",
                                    "phrase": "Named Repository",
                                },
                                {
                                    "phrase": "Subject Repository",
                                    "value": "subject_repository",
                                    "language": "en",
                                },
                                {
                                    "phrase": "Journal Website",
                                    "language": "en",
                                    "value": "this_journal",
                                },
                            ],
                            "location": [
                                "any_website",
                                "institutional_repository",
                                "named_repository",
                                "subject_repository",
                                "this_journal",
                            ],
                        },
                    },
                    {
                        "license": [
                            {
                                "license": "cc_by",
                                "license_phrases": [
                                    {
                                        "phrase": "CC BY",
                                        "language": "en",
                                        "value": "cc_by",
                                    }
                                ],
                            }
                        ],
                        "location": {
                            "location_phrases": [
                                {
                                    "phrase": "Any Repository",
                                    "language": "en",
                                    "value": "any_repository",
                                },
                                {
                                    "value": "institutional_repository",
                                    "language": "en",
                                    "phrase": "Institutional Repository",
                                },
                                {
                                    "value": "named_repository",
                                    "language": "en",
                                    "phrase": "Named Repository",
                                },
                                {
                                    "phrase": "Subject Repository",
                                    "value": "subject_repository",
                                    "language": "en",
                                },
                                {
                                    "phrase": "Journal Website",
                                    "language": "en",
                                    "value": "this_journal",
                                },
                            ],
                            "location": [
                                "any_repository",
                                "institutional_repository",
                                "named_repository",
                                "subject_repository",
                                "this_journal",
                            ],
                            "named_repository": [
                                "PubMed Central",
                                "Research for Development Repository",
                                "ESRC Research Catalogue",
                            ],
                        },
                        "publisher_deposit": [
                            {
                                "repository_metadata": {
                                    "type_phrases": [
                                        {
                                            "phrase": "Disciplinary",
                                            "value": "disciplinary",
                                            "language": "en",
                                        }
                                    ],
                                    "type": "disciplinary",
                                    "name": [
                                        {
                                            "preferred": "name",
                                            "name": "PubMed Central",
                                            "language": "en",
                                            "language_phrases": [
                                                {
                                                    "language": "en",
                                                    "value": "en",
                                                    "phrase": "English",
                                                }
                                            ],
                                            "preferred_phrases": [
                                                {
                                                    "phrase": "Name",
                                                    "language": "en",
                                                    "value": "name",
                                                }
                                            ],
                                        }
                                    ],
                                    "url": "http://www.ncbi.nlm.nih.gov/pmc/",
                                },
                                "system_metadata": {
                                    "uri": "https://v2.sherpa.ac.uk/id/repository/267",
                                    "id": 267,
                                },
                            }
                        ],
                        "additional_oa_fee": "yes",
                        "prerequisites": {
                            "prerequisite_funders": [
                                {
                                    "funder_metadata": {
                                        "groups": [
                                            {
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                            },
                                            {
                                                "name": "Plan S Funders",
                                                "id": 1063,
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1063",
                                                "type": "funder_group",
                                            },
                                        ],
                                        "name": [
                                            {
                                                "name": "Wellcome Trust",
                                                "preferred": "name",
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "country": "gb",
                                        "identifiers": [
                                            {
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "value": "fundref",
                                                        "language": "en",
                                                    }
                                                ],
                                                "identifier": "http://dx.doi.org/10.13039/100004440",
                                            },
                                            {
                                                "identifier": "https://ror.org/029chgv08",
                                                "type_phrases": [
                                                    {
                                                        "value": "ror",
                                                        "language": "en",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "language": "en",
                                                "url": "http://www.wellcome.ac.uk/",
                                            }
                                        ],
                                        "id": 695,
                                    },
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/695",
                                        "id": 695,
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "name": [
                                            {
                                                "preferred_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "name",
                                                        "phrase": "Name",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "name": "British Heart Foundation",
                                                "acronym": "BHF",
                                                "preferred": "name",
                                            }
                                        ],
                                        "groups": [
                                            {
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                            }
                                        ],
                                        "url": [
                                            {
                                                "url": "http://www.bhf.org.uk/",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000274",
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "fundref",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                                "type": "fundref",
                                            },
                                            {
                                                "identifier": "https://ror.org/02wdwnk04",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "language": "en",
                                                        "value": "ror",
                                                    }
                                                ],
                                                "type": "ror",
                                            },
                                        ],
                                        "id": 18,
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "value": "gb",
                                                "language": "en",
                                            }
                                        ],
                                        "country": "gb",
                                    },
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/18",
                                        "id": 18,
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "groups": [
                                            {
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                            }
                                        ],
                                        "name": [
                                            {
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "language": "en",
                                                        "value": "name",
                                                    }
                                                ],
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "language": "en",
                                                "name": "Versus Arthritis",
                                                "preferred": "name",
                                            }
                                        ],
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "notes": "Launched in September 2018, following the legal merger of the two leading arthritis charities in the UK, Arthritis Research UK and Arthritis Care",
                                        "country": "gb",
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000341",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "value": "fundref",
                                                        "language": "en",
                                                    }
                                                ],
                                                "type": "fundref",
                                            },
                                            {
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "ror",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                                "identifier": "https://ror.org/02jkpm469",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "url": "https://www.versusarthritis.org/",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "id": 14,
                                    },
                                    "system_metadata": {
                                        "id": 14,
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/14",
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "country": "gb",
                                        "groups": [
                                            {
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1061",
                                                "type": "funder_group",
                                                "name": "UK Research and Innovation",
                                                "id": 1061,
                                            },
                                            {
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                                "id": 1059,
                                                "name": "Europe PMC Funders' Group",
                                            },
                                            {
                                                "name": "Plan S Funders",
                                                "id": 1063,
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1063",
                                            },
                                        ],
                                        "identifiers": [
                                            {
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "value": "fundref",
                                                        "language": "en",
                                                    }
                                                ],
                                                "identifier": "http://dx.doi.org/10.13039/501100000268",
                                            },
                                            {
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "language": "en",
                                                        "value": "ror",
                                                    }
                                                ],
                                                "type": "ror",
                                                "identifier": "https://ror.org/00cwqg982",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "url": "http://www.bbsrc.ac.uk/home/home.aspx",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "name": [
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "language": "en",
                                                        "value": "name",
                                                    }
                                                ],
                                                "acronym": "BBSRC",
                                                "preferred": "name",
                                                "name": "Biotechnology and Biological Sciences Research Council",
                                            }
                                        ],
                                        "id": 709,
                                    },
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/709",
                                        "id": 709,
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "id": 925,
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/925",
                                    },
                                    "funder_metadata": {
                                        "id": 925,
                                        "identifiers": [
                                            {
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "value": "fundref",
                                                        "language": "en",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                                "identifier": "http://dx.doi.org/10.13039/501100007903",
                                            },
                                            {
                                                "identifier": "https://ror.org/0055acf80",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "value": "ror",
                                                        "language": "en",
                                                    }
                                                ],
                                                "type": "ror",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "url": "https://bloodcancer.org.uk/",
                                            }
                                        ],
                                        "country": "gb",
                                        "notes": "Formerly Bloodwise. Prior to that it was  Leukaemia & Lymphoma Research.",
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "name": [
                                            {
                                                "name": "Blood Cancer UK",
                                                "preferred": "name",
                                                "preferred_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "name",
                                                        "phrase": "Name",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                            }
                                        ],
                                        "groups": [
                                            {
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "id": 1059,
                                                "name": "Europe PMC Funders' Group",
                                            }
                                        ],
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "identifiers": [
                                            {
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "value": "fundref",
                                                        "language": "en",
                                                    }
                                                ],
                                                "type": "fundref",
                                                "identifier": "http://dx.doi.org/10.13039/100000865",
                                            },
                                            {
                                                "identifier": "https://ror.org/0456r8d26",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "language": "en",
                                                        "value": "ror",
                                                    }
                                                ],
                                                "type": "ror",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "language": "en",
                                                "url": "http://www.gatesfoundation.org/",
                                            }
                                        ],
                                        "id": 961,
                                        "name": [
                                            {
                                                "preferred": "name",
                                                "name": "Bill & Melinda Gates Foundation",
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "language": "en",
                                                        "value": "name",
                                                    }
                                                ],
                                            }
                                        ],
                                        "country_phrases": [
                                            {
                                                "value": "us",
                                                "language": "en",
                                                "phrase": "United States of America",
                                            }
                                        ],
                                        "country": "us",
                                        "groups": [
                                            {
                                                "id": 1063,
                                                "name": "Plan S Funders",
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1063",
                                            }
                                        ],
                                    },
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/961",
                                        "id": 961,
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/19",
                                        "id": 19,
                                    },
                                    "funder_metadata": {
                                        "country_phrases": [
                                            {
                                                "value": "gb",
                                                "language": "en",
                                                "phrase": "United Kingdom",
                                            }
                                        ],
                                        "country": "gb",
                                        "groups": [
                                            {
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000289",
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "value": "fundref",
                                                        "language": "en",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                            },
                                            {
                                                "identifier": "https://ror.org/054225q67",
                                                "type": "ror",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "language": "en",
                                                        "value": "ror",
                                                    }
                                                ],
                                            },
                                        ],
                                        "url": [
                                            {
                                                "url": "http://www.cancerresearchuk.org/",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "name": [
                                            {
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "name": "Cancer Research UK",
                                                "preferred": "name",
                                            }
                                        ],
                                        "id": 19,
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/16",
                                        "id": 16,
                                    },
                                    "funder_metadata": {
                                        "name": [
                                            {
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "preferred": "name",
                                                "acronym": "CSO",
                                                "name": "Chief Scientist Office, Scottish Executive",
                                            }
                                        ],
                                        "groups": [
                                            {
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                            }
                                        ],
                                        "url": [
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "url": "https://www.cso.scot.nhs.uk/",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000589",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "language": "en",
                                                        "value": "fundref",
                                                    }
                                                ],
                                                "type": "fundref",
                                            },
                                            {
                                                "type": "ror",
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "ror",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "identifier": "https://ror.org/01613vh25",
                                            },
                                        ],
                                        "id": 16,
                                        "country_phrases": [
                                            {
                                                "language": "en",
                                                "value": "gb",
                                                "phrase": "United Kingdom",
                                            }
                                        ],
                                        "country": "gb",
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "name": [
                                            {
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "name": "Department of Health & Social Care (2014)",
                                                "acronym": "DH",
                                                "preferred": "name",
                                            },
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "preferred_phrases": [
                                                    {
                                                        "value": "name",
                                                        "language": "en",
                                                        "phrase": "Name",
                                                    }
                                                ],
                                                "acronym": "NIHR",
                                                "preferred": "name",
                                                "name": "National Institute for Health Research (2014)",
                                            },
                                        ],
                                        "groups": [
                                            {
                                                "id": 1059,
                                                "name": "Europe PMC Funders' Group",
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                            }
                                        ],
                                        "id": 943,
                                        "url": [
                                            {
                                                "url": "https://www.gov.uk/government/organisations/department-of-health-and-social-care",
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                            },
                                            {
                                                "url": "https://www.nihr.ac.uk/",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            },
                                        ],
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000272",
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "value": "fundref",
                                                        "language": "en",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                            },
                                            {
                                                "identifier": "https://ror.org/0187kwz08",
                                                "type": "ror",
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "ror",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                            },
                                        ],
                                        "country": "gb",
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                    },
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/943",
                                        "id": 943,
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/410",
                                        "id": 410,
                                    },
                                    "funder_metadata": {
                                        "groups": [
                                            {
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                            }
                                        ],
                                        "name": [
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "acronym": "DMT",
                                                "preferred": "name",
                                                "name": "Dunhill Medical Trust",
                                            }
                                        ],
                                        "country": "gb",
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "id": 410,
                                        "url": [
                                            {
                                                "url": "https://dunhillmedical.org.uk/",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000377",
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "value": "fundref",
                                                        "language": "en",
                                                    }
                                                ],
                                            },
                                            {
                                                "identifier": "https://ror.org/05ayqqv15",
                                                "type_phrases": [
                                                    {
                                                        "value": "ror",
                                                        "language": "en",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                            },
                                        ],
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/31",
                                        "id": 31,
                                    },
                                    "funder_metadata": {
                                        "country_phrases": [
                                            {
                                                "phrase": "Belgium",
                                                "value": "be",
                                                "language": "en",
                                            }
                                        ],
                                        "country": "be",
                                        "url": [
                                            {
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "url": "http://erc.europa.eu/",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "value": "fundref",
                                                        "language": "en",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                                "identifier": "http://dx.doi.org/10.13039/501100000781",
                                            },
                                            {
                                                "type_phrases": [
                                                    {
                                                        "value": "ror",
                                                        "language": "en",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                                "identifier": "https://ror.org/0472cxd90",
                                            },
                                        ],
                                        "id": 31,
                                        "groups": [
                                            {
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                            }
                                        ],
                                        "name": [
                                            {
                                                "name": "European Research Council",
                                                "acronym": "ERC",
                                                "preferred": "name",
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "language": "en",
                                                        "value": "name",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                            }
                                        ],
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "groups": [
                                            {
                                                "id": 1061,
                                                "name": "UK Research and Innovation",
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1061",
                                            },
                                            {
                                                "id": 1059,
                                                "name": "Europe PMC Funders' Group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                            },
                                            {
                                                "name": "Plan S Funders",
                                                "id": 1063,
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1063",
                                            },
                                        ],
                                        "country": "gb",
                                        "url": [
                                            {
                                                "url": "http://www.mrc.ac.uk/index.htm",
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "value": "fundref",
                                                        "language": "en",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                                "identifier": "http://dx.doi.org/10.13039/501100000265",
                                            },
                                            {
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "ror",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                                "identifier": "https://ror.org/03x94j517",
                                            },
                                        ],
                                        "id": 705,
                                        "name": [
                                            {
                                                "acronym": "MRC",
                                                "preferred": "name",
                                                "name": "Medical Research Council",
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "preferred_phrases": [
                                                    {
                                                        "value": "name",
                                                        "language": "en",
                                                        "phrase": "Name",
                                                    }
                                                ],
                                            }
                                        ],
                                    },
                                    "system_metadata": {
                                        "id": 705,
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/705",
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "id": 562,
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/562",
                                    },
                                    "funder_metadata": {
                                        "country": "gb",
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "language": "en",
                                                "value": "gb",
                                            }
                                        ],
                                        "id": 562,
                                        "url": [
                                            {
                                                "url": "http://www.mndassociation.org/",
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "language": "en",
                                                        "value": "fundref",
                                                    }
                                                ],
                                                "type": "fundref",
                                                "identifier": "http://dx.doi.org/10.13039/501100000406",
                                            },
                                            {
                                                "identifier": "https://ror.org/02gq0fg61",
                                                "type": "ror",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "language": "en",
                                                        "value": "ror",
                                                    }
                                                ],
                                            },
                                        ],
                                        "groups": [
                                            {
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                            }
                                        ],
                                        "name": [
                                            {
                                                "acronym": "MND Association",
                                                "preferred": "name",
                                                "name": "Motor Neuron Disease Association",
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "language": "en",
                                                        "value": "name",
                                                    }
                                                ],
                                            }
                                        ],
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "country": "gb",
                                        "country_phrases": [
                                            {
                                                "phrase": "United Kingdom",
                                                "value": "gb",
                                                "language": "en",
                                            }
                                        ],
                                        "id": 411,
                                        "identifiers": [
                                            {
                                                "identifier": "http://dx.doi.org/10.13039/501100000304",
                                                "type": "fundref",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "FundRef DOI",
                                                        "language": "en",
                                                        "value": "fundref",
                                                    }
                                                ],
                                            },
                                            {
                                                "type_phrases": [
                                                    {
                                                        "value": "ror",
                                                        "language": "en",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                                "identifier": "https://ror.org/02417p338",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "value": "en",
                                                        "language": "en",
                                                    }
                                                ],
                                                "url": "http://www.parkinsons.org.uk/",
                                            }
                                        ],
                                        "groups": [
                                            {
                                                "id": 1059,
                                                "name": "Europe PMC Funders' Group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                                "type": "funder_group",
                                            },
                                            {
                                                "id": 1067,
                                                "name": "Association of Medical Research Charities",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1067",
                                                "type": "funder_group",
                                            },
                                        ],
                                        "name": [
                                            {
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "name": "Parkinson's UK",
                                                "preferred": "name",
                                            }
                                        ],
                                    },
                                    "system_metadata": {
                                        "id": 411,
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/411",
                                    },
                                },
                                {
                                    "funder_metadata": {
                                        "name": [
                                            {
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "value": "en",
                                                        "language": "en",
                                                        "phrase": "English",
                                                    }
                                                ],
                                                "name": "Telethon Foundation",
                                                "preferred": "name",
                                            },
                                            {
                                                "name": "Fondazione Telethon",
                                                "preferred": "name",
                                                "preferred_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "name",
                                                        "phrase": "Name",
                                                    }
                                                ],
                                                "language": "it",
                                                "language_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "it",
                                                        "phrase": "Italian",
                                                    }
                                                ],
                                            },
                                        ],
                                        "groups": [
                                            {
                                                "name": "Europe PMC Funders' Group",
                                                "id": 1059,
                                                "type": "funder_group",
                                                "uri": "https://v2.sherpa.ac.uk/id/funder_group/1059",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "fundref",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                                "type": "fundref",
                                                "identifier": "http://dx.doi.org/10.13039/501100002426",
                                            },
                                            {
                                                "type_phrases": [
                                                    {
                                                        "value": "ror",
                                                        "language": "en",
                                                        "phrase": "ROR ID",
                                                    }
                                                ],
                                                "type": "ror",
                                                "identifier": "https://ror.org/04xraxn18",
                                            },
                                        ],
                                        "url": [
                                            {
                                                "url": "https://www.telethon.it/en/",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            },
                                            {
                                                "url": "http://www.telethon.it/",
                                                "language_phrases": [
                                                    {
                                                        "value": "it",
                                                        "language": "en",
                                                        "phrase": "Italian",
                                                    }
                                                ],
                                                "language": "it",
                                            },
                                        ],
                                        "id": 325,
                                        "country_phrases": [
                                            {
                                                "phrase": "Italy",
                                                "language": "en",
                                                "value": "it",
                                            }
                                        ],
                                        "country": "it",
                                    },
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/325",
                                        "id": 325,
                                    },
                                },
                                {
                                    "system_metadata": {
                                        "uri": "https://v2.sherpa.ac.uk/id/funder/1333",
                                        "id": 1333,
                                    },
                                    "funder_metadata": {
                                        "name": [
                                            {
                                                "name": "National Institute for Health and Care Research (2022)",
                                                "acronym": "NIHR",
                                                "preferred": "name",
                                                "preferred_phrases": [
                                                    {
                                                        "phrase": "Name",
                                                        "value": "name",
                                                        "language": "en",
                                                    }
                                                ],
                                                "language": "en",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                            }
                                        ],
                                        "country": "gb",
                                        "country_phrases": [
                                            {
                                                "language": "en",
                                                "value": "gb",
                                                "phrase": "United Kingdom",
                                            }
                                        ],
                                        "id": 1333,
                                        "url": [
                                            {
                                                "url": "https://www.nihr.ac.uk/",
                                                "language_phrases": [
                                                    {
                                                        "phrase": "English",
                                                        "language": "en",
                                                        "value": "en",
                                                    }
                                                ],
                                                "language": "en",
                                            }
                                        ],
                                        "identifiers": [
                                            {
                                                "type": "ror",
                                                "type_phrases": [
                                                    {
                                                        "phrase": "ROR ID",
                                                        "value": "ror",
                                                        "language": "en",
                                                    }
                                                ],
                                                "identifier": "https://ror.org/0187kwz08",
                                            },
                                            {
                                                "identifier": "10.13039/501100000272",
                                                "type_phrases": [
                                                    {
                                                        "language": "en",
                                                        "value": "fundref",
                                                        "phrase": "FundRef DOI",
                                                    }
                                                ],
                                                "type": "fundref",
                                            },
                                        ],
                                    },
                                },
                            ]
                        },
                        "conditions": [
                            "Published source must be acknowledged with citation"
                        ],
                        "id": 4850,
                        "article_version": ["published"],
                        "additional_oa_fee_phrases": [
                            {"phrase": "Yes", "language": "en", "value": "yes"}
                        ],
                        "article_version_phrases": [
                            {
                                "phrase": "Published",
                                "value": "published",
                                "language": "en",
                            }
                        ],
                    },
                ],
                "publication_count": 1929,
                "urls": [
                    {
                        "description": "Open access",
                        "url": "https://www.elsevier.com/about/open-science/open-access",
                    },
                    {
                        "description": "Open access licenses",
                        "url": "https://www.elsevier.com/about/policies/open-access-licenses",
                    },
                ],
                "id": 3323,
            },
        ],
        "type_phrases": [{"phrase": "Journal", "language": "en", "value": "journal"}],
        "type": "journal",
        "system_metadata": {
            "uri": "https://v2.sherpa.ac.uk/id/publication/23055",
            "publicly_visible": "yes",
            "publicly_visible_phrases": [
                {"phrase": "Yes", "value": "yes", "language": "en"}
            ],
            "date_created": "2012-05-18 10:41:21",
            "id": 23055,
            "date_modified": "2022-04-27 15:41:55",
        },
    }
